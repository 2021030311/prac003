/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;
import controlador.*;
/**
 *
 * @author Jonha
 */
public class Bomba {
    private int numBomba, capacidadBomba;
    private int acumuladorLitros;
    private Gasolina gaso;
    
    public Bomba(){
        this.numBomba = 0;
        this.capacidadBomba = 0;
        this.acumuladorLitros = 0;
        gaso = new Gasolina();
    }
    
    public Bomba(int numBomba,int capacidadBomba,int acumuladorLitros, Gasolina gaso){
        this.numBomba = numBomba;
        this.capacidadBomba = capacidadBomba;
        this.acumuladorLitros = acumuladorLitros;
        this.gaso = gaso;
    }
    
    public Bomba(Bomba b){
        this.numBomba = b.numBomba;
        this.capacidadBomba = b.capacidadBomba;
        this.acumuladorLitros = b.acumuladorLitros;
        this.gaso = b.gaso;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public int getCapacidadBomba() {
        return capacidadBomba;
    }

    public void setCapacidadBomba(int capacidadBomba) {
        this.capacidadBomba = capacidadBomba;
    }

    public double getAcumuladorLitros() {
        return acumuladorLitros;
    }

    public void setAcumuladorLitros(int acumuladorLitros) {
        this.acumuladorLitros = acumuladorLitros;
    }

    public Gasolina getGaso() {
        return gaso;
    }

    public void setGaso(Gasolina gaso) {
        this.gaso = gaso;
    }
    
    public void iniciarBomba(int numBomba, int acumuladorLitros, int capacidadBomba, Gasolina gaso) {
        this.numBomba = numBomba;
        this.capacidadBomba = capacidadBomba;
        this.acumuladorLitros = acumuladorLitros;
        this.gaso = gaso;
    }
    
    public int getInventarioGas(){
        return capacidadBomba - acumuladorLitros;
    }
    
    public float getVentasTotal(){
        return this.acumuladorLitros * gaso.getPrecio();
    }
    
    public float VenderGasolina(int VenderGas){
        if(VenderGas > getInventarioGas()){
            return 0;
        }
        acumuladorLitros = acumuladorLitros + VenderGas;
        return (float) VenderGas * gaso.getPrecio();
    }
}
