/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Jonha
 */
public class Gasolina {
    private int idGas;
    private String tipoGas;
    private float precio;
    
    public Gasolina(){
        idGas = 0;
        tipoGas = "";
        precio = 0;
    }
    
    public Gasolina(int idGas, String tipoGas, float precio){
        this.idGas = idGas;
        this.precio = precio;
        this.tipoGas = tipoGas;
    }
    
    public Gasolina(Gasolina gasol){
        this.idGas = gasol.idGas;
        this.precio = gasol.precio;
        this.tipoGas = gasol.tipoGas;
    }
    
    public int getIdGas(){
        return idGas;
    }
    
    public void setIdGas(int idGas){
        this.idGas = idGas;
    }
    
    public String getTipoGas(){
        return tipoGas;
    }
    
    public void setTipoGas(String tipoGas){
        this.tipoGas = tipoGas;
    }
    
    public float getPrecio(){
        return precio;
    }
    
    public void setPrecio(float precio){
        this.precio = precio;
    }
}
