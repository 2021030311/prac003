/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *///SE REVISO EL PROYECTO
//SE VOLVIO A REVISAR EL PROYECTO
package controlador;
import modelo.Gasolina;
import modelo.Bomba;
import vista.dlgBomba;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
/**
 *
 * @author Jonha
 */
public class Controlador implements ActionListener {
    
    private Bomba bomb;
    private dlgBomba vista;
    
    public Controlador(Bomba bomb, dlgBomba vista){
        this.bomb = bomb;
        this.vista = vista;
        vista.btnIniciarBomba.addActionListener(this);
        vista.btnRegister.addActionListener(this);
    }
    
     private void iniciarVista(){
        vista.setTitle(" == Venta Gasolina == ");
        vista.setSize(600, 450);
        vista.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == vista.btnIniciarBomba){
           // String tipo;
           // String val = vista.txtNuBomb.getText();
            
            try {
                if((vista.txtNuBomb.getText().equals(""))){
                    throw new IllegalArgumentException("PRIMERO INGRESE UN NUMERO DE BOMBA");
                }
                
                if(Integer.parseInt(vista.txtNuBomb.getText()) <= 0){
                    throw new IllegalArgumentException("ERROR INTENTE DE NUEVO");
                }
                
                bomb.iniciarBomba(Integer.parseInt(vista.txtNuBomb.getText()), 0, Integer.parseInt(vista.txtCapacidad.getText()), new Gasolina((vista.ComboBoxTipoGas.getSelectedIndex() + 1), vista.ComboBoxTipoGas.getSelectedItem().toString(), Float.parseFloat(vista.txtPrecio.getText())));
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex2) {
                JOptionPane.showMessageDialog(vista, ex2.getMessage());
                return;
            }
            
            vista.btnRegister.setEnabled(true);
            vista.txtVentaGas.setEnabled(true);
            
            vista.txtNuBomb.setEnabled(false);
            vista.ComboBoxTipoGas.setEnabled(false);
            vista.btnIniciarBomba.setEnabled(false);
            vista.sliderCapacidad.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnRegister){
            float total = 0;
            try {
                
                if((vista.txtVentaGas.getText()).equals("")){
                    throw new IllegalArgumentException("INGRESE LA GASOLINA A VENDER");
                }
                
                if(Integer.parseInt(vista.txtVentaGas.getText()) < 0){
                    throw new IllegalArgumentException("ERROR, INTENTE DE NUEVO");
                }
                
                total = bomb.VenderGasolina(Integer.parseInt(vista.txtVentaGas.getText()));
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex2) {
                JOptionPane.showMessageDialog(vista, ex2.getMessage());
                return;
            }
            if(total == 0){
                JOptionPane.showMessageDialog(vista, "Gasolina insuficiente");
                return;
            }
            
           // if( bomb.getInventarioGas() == 0){
           //     JOptionPane.showMessageDialog(vista, "Gasolina Insuficiente");
           //     return;
           // }
            
    
            JOptionPane.showMessageDialog(vista, "Venta realizada");
            vista.txtNumVentas.setText(Integer.toString(Integer.parseInt(vista.txtNumVentas.getText())+1));
            vista.txtCapacidad.setText(vista.txtCapacidad.getText());
            vista.txtCapacidad.setText(Integer.toString(Integer.parseInt(vista.txtCapacidad.getText()) - Integer.parseInt(vista.txtVentaGas.getText())));
            vista.sliderCapacidad.setValue(Integer.parseInt(vista.txtCapacidad.getText()));
            vista.txtCosto.setText(Float.toString(total));
            vista.txtTotalVentas.setText(Float.toString(bomb.getVentasTotal()));
        }
    }
    public static void main(String[] args) {
        // TODO code application logic here
        Bomba bomb = new Bomba();
        Gasolina gas = new Gasolina();
        dlgBomba vista = new dlgBomba(new JFrame(), true);
        
        Controlador contra = new Controlador(bomb, vista);
        contra.iniciarVista();
    }
}
